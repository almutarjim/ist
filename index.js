async function fetchWeatherData() {
    const response = await fetch('https://api.open-meteo.com/v1/forecast?latitude=24.48&longitude=39.55&daily=temperature_2m_max,temperature_2m_min&timezone=auto');
    return await response.json();
}

function createWeatherCard(time, minTemp) {
    const date = new Date(time);
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return `
        <div class="weatherCard">
            <div class="time">
                <p class="day">${days[date.getDay()]}</p>
                <p class="date">${date.getDate()} ${date.toLocaleString('default', { month: 'short' })}</p>
            </div>
            <p class="temp">${Math.round(minTemp)}°</p>
        </div>
    `;
}

const determineSeason = avgTemp => {
    let season = avgTemp < 18 ? 'Winter' : avgTemp < 27 ? 'Spring' : 'Summer';
    document.getElementById('icon').src = `${season}.webp`;
    document.body.style.backgroundImage = `linear-gradient(to top, rgba(0, 0, 0, 0.7), url('${season}bg.webp'))`;
    return season;
};

async function displayWeatherData() {
    try {
        const { daily: { time, temperature_2m_min } } = await fetchWeatherData();
        document.getElementById('main').innerHTML = time.map((time, i) => createWeatherCard(time, temperature_2m_min[i])).join('');
        document.getElementById('season').textContent = determineSeason(temperature_2m_min[0]);
    } catch (error) {
        console.error('Error:', error);
    }
}

displayWeatherData();